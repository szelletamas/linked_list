#include <stdio.h>
#include <stdlib.h>

typedef struct rectangle_t{
    int a;
    int b;
    int area;
    int perimeter;
}rectangle_t;

typedef struct node_t{
    rectangle_t rectangle;
    struct node_t* next;
    struct node_t* prev;
    int id;
}node_t;

typedef struct list_t{
    node_t* head;
    int size;
}list_t;

void init_list(list_t* list);
void deinit_list(list_t* list);

void list_push(list_t* list,rectangle_t rectangle);

void print_list(list_t list);

void re_id(list_t list);

rectangle_t rectangle_init(int a, int b);

void delete_by_id(list_t* list,int id);

void swap(list_t list,int a,int b);

void list_sort(list_t list);

int main()
{

    list_t list;
    init_list(&list);

    list_push(&list,rectangle_init(3,4));
    list_push(&list,rectangle_init(32,2));
    list_push(&list,rectangle_init(1,4));
    list_push(&list,rectangle_init(35,41));
    list_push(&list,rectangle_init(42,4));



    list_sort(list);

    print_list(list);

    deinit_list(&list);
    return 0;
}

void list_sort(list_t list){
    if(list.size<=1){
        return;
    }

    int swap_counter=1;

    while(swap_counter!=0){
        swap_counter=0;

        node_t* actual = list.head;

        while(actual->next!=NULL){
            if(actual->rectangle.area>actual->next->rectangle.area){
                swap_counter++;
                swap(list,actual->id,actual->next->id);

            }

            actual=actual->next;
        }
    }
}


void swap(list_t list,int a,int b){
    if(a>=list.size || a < 0){
            return;
    }

    if(b>=list.size || b < 0){
            return;
    }
    node_t* first=list.head;

    while(first->id!=a){
            first=first->next;
    }

    node_t* second=list.head;

    while(second->id!=b){
            second=second->next;
    }

    rectangle_t temp=first->rectangle;

    first->rectangle=second->rectangle;

    second->rectangle=temp;

}

void re_id(list_t list){
    if(list.head==NULL){
        return;
    }

    node_t* item=list.head;

    int i=0;
    while(item!=NULL){
        item->id=i;
        item=item->next;
        i++;
    }
}

void print_list(list_t list){
    if(list.head==NULL){
        return;
    }

    node_t* item=list.head;

    while(item!=NULL){
     printf("lista %d-edik eleme: a: %d\tb: %d\tkerulet: %d\tterulet: %d\n",
               item->id,
               item->rectangle.a,
               item->rectangle.b,
               item->rectangle.perimeter,
               item->rectangle.area);

    item=item->next;
    }
}

void delete_by_id(list_t* list,int id){
    if(!list || !list->head){
        return;
    }

    if(id>=list->size || id < 0){
        return;
    }

    if(id==0){
        node_t* del=list->head;
        list->head=del->next;
        del->next->prev=NULL;
        free(del);
        re_id(*list);
        list->size--;
        return;
    }

    if(list->size-1==id){
        node_t* del=list->head;

        while(del->next!=NULL){
            del=del->next;
        }

        del->prev->next=NULL;
        free(del);
        list->size--;
        return;
    }

    node_t* del=list->head;

    while(del->id!=id){
            del=del->next;
    }

    del->next->prev=del->prev;
    del->prev->next=del->next;

    free(del);
    re_id(*list);
    list->size--;
}

rectangle_t rectangle_init(int a, int b){
    rectangle_t rectangle;
    rectangle.a=a;
    rectangle.b=b;
    rectangle.area=a*b;
    rectangle.perimeter=2*(a+b);
    return rectangle;
}

void list_push(list_t* list,rectangle_t rectangle){
    if(!list){
        return;
    }

    node_t* new_item=(node_t*) malloc(sizeof(node_t));
    new_item->id=list->size;
    new_item->next=NULL;
    new_item->prev=NULL;
    new_item->rectangle=rectangle;

    if(list->head==NULL){
        list->head=new_item;
        list->size=1;
        return;
    }

    node_t* last_item=list->head;

    while(last_item->next!=NULL){
        last_item=last_item->next;
    }

    last_item->next=new_item;
    new_item->prev=last_item;
    list->size++;
}

void init_list(list_t* list){
    if(!list){
        return;
    }

    list->head=NULL;
    list->size=0;
}

void deinit_list(list_t* list){
    if(!list){
        return;
    }

    list->size=0;

    node_t* del=list->head;
    node_t* next_del=list->head;

    while(del!=NULL){
        next_del=next_del->next;
        free(del);
        del=next_del;
    }

    list->head=NULL;
}
